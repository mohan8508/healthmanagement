import React from 'react';
import { StyleSheet, View, KeyboardAvoidingView, Picker, ScrollView, TouchableOpacity, Keyboard, BackHandler, Platform, AsyncStorage, StatusBar, AppState } from 'react-native';
import { Container, Content, Button, Text, Header, Body, ListItem, Item, Input, Form, CheckBox, Spinner } from 'native-base';
import Toast from 'react-native-easy-toast';
import firebase from 'react-native-firebase'

export default class Register extends React.Component {
    static navigationOptions = {
        header: null,
        drawerLockMode: 'locked-closed'
    };
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            password: '',
            confirmPassword: '',
            phone: ''
        }
    }


    componentDidMount() {
        firebase.messaging()
            .hasPermission()
            .then(enabled => {
                if (!enabled) {
                    this._getPermission();
                }
            });

        BackHandler.addEventListener('hardwareBackPress', this.backPressed);
    }
    _getPermission = () => {
        firebase.messaging()
            .requestPermission()
            .catch(error => {
                // User has rejected permissions
                this._getPermission();
            });
    };


    componentWillUnmount() {

        BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
    }

    onSelectedItemsChange = (selectedItems) => {
        this.setState({ selectedItems });
    }

    backPressed = () => {
        return this.props.navigation.navigate('Login')
    }

    onPressSignup = async () => {
        Keyboard.dismiss()
        this.setState({ emailerror: false, namerror: false, passworderror: false, confirmpasserror: false })
        let count = 0
        const { name, email, password, confirmPassword, phone } = this.state
        const isValidFName = name.match(/^[a-zA-Z]+$/)
        const isValidEM = email.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)
        const isValidPassword = password.match(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/)
        if (name === '') {
            this.setState({ namerror: true })
            count++
        }
        if (email === '') {
            this.setState({ emailerror: true })
            count++
        }
        if (password === '') {
            this.setState({ passworderror: true })
            count++
        }
        if (confirmPassword === '') {
            this.setState({ confirmpasserror: true })
            count++
        }
        if (name === '' && email === '' && password === '' && confirmPassword === '') {
            this.refs.toast.show('Please enter all required fields');
            this.setState({ emailerror: true, namerror: true, passworderror: true, confirmpasserror: true })
        } else if (count > 1) {
            this.refs.toast.show('Please enter missing fields');
        }
        else if (!isValidFName) {
            this.setState({ namerror: true })
            this.refs.toast.show('Please enter valid firstname');
        }
        else if (!isValidEM) {
            this.setState({ emailerror: true })

            this.refs.toast.show('Please enter the valid email');
        }
        else if (!isValidPassword) {
            this.setState({ passworderror: true })
            this.refs.toast.show('Your password must have minimum 8 character that contains uppercase, lowercase, number and special character');
        }
        else if (password !== confirmPassword) {
            this.refs.toast.show('Passwords doesn\'t match');
        }
        else if (phone === '') {
            this.refs.toast.show('please enter your mobile number');
        }
        else if (isNaN(phone) || phone.length !== 10) {
            this.refs.toast.show('please enter your valid mobile number');
        }
        else if (isValidFName && isValidEM && isValidPassword) {
            this.setState({ loading: true })
            firebase.auth().createUserWithEmailAndPassword(email, password).then(async (successvalue) => {
                if (successvalue) {
                    const collection = firebase.firestore().collection('Users')
                    collection.where("email", "==", email).get().then(async (success) => {
                        if (success.docs.length > 0) {
                            this.setState({ loading: false })
                            this.refs.toast.show("already exists");
                        } else {
                            collection.doc(successvalue.user._user.uid).set({
                                displayName: name,
                                email: email,
                                phone:phone,
                                active:false,
                                loggedin:false,
                                token:''
                            }).then(async (successdata) => {
                                this.refs.toast.show('Registered successfully, Please wait while your activation is being processed by admin');
                                this.props.navigation.navigate('Login')
                            })
                        }
                    }).catch((err) => {
                        this.setState({ loading: true })
                        this.refs.toast.show(err.toString());
                    })
                }

            }).catch((error) => {
                this.setState({ loading: false })
                this.refs.toast.show(error.toString());
            }
            )
        }
    }

    termsOfService = (type) => {
        if (type === "Terms") {
            this.setState({ termsofservice: !this.state.termsofservice })
        } else {
            this.setState({ privacy: !this.state.privacy })
        }
    }

    render() {
        return (
            <Container style={styles.container}>
                <StatusBar barStyle="dark-content" backgroundColor="#0d98ba" />
                <Content>
                    <View>
                        <Text style={[styles.heading, {color:'#fff', marginTop: 100 }]}>REGISTER</Text>
                    </View>
                    <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ width: '90%', alignItems: 'center', justifyContent: 'center' }}>
                            <Item regular style={{ marginTop: 10, borderColor: this.state.namerror ? "red" : '#0d98ba', borderWidth: 2 }}>
                                <Input style={{backgroundColor:'#ccf2ff'}} placeholderTextColor="#000066"  placeholder="First name*" onChangeText={(text) => this.setState({ name: text })} />
                            </Item>
                            <Item regular style={{ marginTop: 10, marginTop: 10, borderColor: this.state.emailerror ? "red" : '#0d98ba', borderWidth: 2 }}>
                                <Input style={{backgroundColor:'#ccf2ff'}} placeholderTextColor="#000066"  placeholder="Email*" autoCapitalize='none' keyboardType="email-address" onChangeText={(text) => this.setState({ email: text })} />
                            </Item>
                            <Item regular style={{ marginTop: 10, marginTop: 10, borderColor: this.state.passworderror ? "red" : '#0d98ba', borderWidth: 2 }}>
                                <Input style={{backgroundColor:'#ccf2ff'}} placeholderTextColor="#000066"  placeholder="Password*" secureTextEntry={true} onChangeText={(text) => this.setState({ password: text })} />
                            </Item>
                            <Item regular style={{ marginTop: 10, marginTop: 10, borderColor: this.state.confirmpasserror ? "red" : '#0d98ba', borderWidth: 2 }}>
                                <Input style={{backgroundColor:'#ccf2ff'}} placeholderTextColor="#000066"  placeholder="Confirm Password*" secureTextEntry={true} onChangeText={(text) => this.setState({ confirmPassword: text })} />
                            </Item>

                            <Item regular style={[styles.row, { marginTop: 10 }]}>
                                <Input style={{backgroundColor:'#ccf2ff'}} placeholderTextColor="#000066"  placeholder="Phone Number" onChangeText={(text) => this.setState({ phone: text })} />
                            </Item>
                            {!this.state.loading && <TouchableOpacity onPress={this.onPressSignup} style={styles.signInBig} >
                                <Text style={styles.signinText1}>SIGN UP</Text>
                            </TouchableOpacity>}
                            {this.state.loading && <View>
                                <Spinner color={'#0d98ba'} />
                            </View>}
                            {!this.state.loading && <Text onPress={() => this.props.navigation.navigate('Login')} style={styles.signupStyle}>SIGN IN</Text>}
                        </View>
                    </View>
                </Content>
                <Toast ref="toast" />
            </Container>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#66d9ff'
    },
    content: {
        paddingHorizontal: '2%',
        paddingVertical: '20%'

    },
    sociallogin: {
        flexDirection: 'row',
        marginTop: 15,
        marginBottom: '6%',
        height: 'auto',
    },
    signInBig: {
        width: '60%',
        marginTop: 25,
        borderRadius: 10,
        backgroundColor: '#0d98ba'
    },
    signinText1: {
        fontSize: 18,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 8,
        marginBottom: 8,
        color: "#fff",
        textAlign: 'center',
        fontWeight: 'bold'
    },
    row: {
        // marginBottom: '2%',
        borderColor: '#0d98ba', borderWidth: 2
        // , backgroundColor: '#0d98ba0a'
    },
    dropdown: {
        borderColor: '#0d98ba', borderWidth: 2
    },
    PhoneInput: {
        padding: 15
    },
    signup: {
        marginTop: '3%',
        fontSize: 17,
        color: '#86c3ef',
        marginBottom: '10%'
    },
    textStyle: {
        textAlign: 'center',
        color: '#ffff',
        fontWeight: '500',
        fontSize: 18,
        paddingTop: '12%'
    },
    socialbuttonstyle: {
        width: '86%',
        borderRadius: 3,
        height: '7%',
        justifyContent: 'center',
        backgroundColor: '#0d98ba',
        paddingBottom: '5%'
    },
    heading: {
        color: '#0d98ba',
        fontSize: 25,
        textAlign: 'center',
    },
    signupStyle: {
        justifyContent: 'center',
        textAlign: 'center',
        color: '#fff',
        fontSize: 18,
        marginTop: 10,
        paddingBottom: 20
    },
    signupText: {
        width: '86%',
        borderRadius: 3,
        height: '15%',
        justifyContent: 'center',
        marginBottom: '10%'

    },
})