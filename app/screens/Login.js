import React from 'react';
import { StyleSheet, View, Keyboard, Alert, BackHandler, KeyboardAvoidingView, ScrollView, AsyncStorage, TouchableOpacity, StatusBar, Platform, AppState } from 'react-native';
import { Button, Text, Content, Container, Item, Input, Spinner, Form, Header } from 'native-base';
import Toast from 'react-native-easy-toast';
import firebase from 'react-native-firebase'

export default class Login extends React.Component {
    static navigationOptions = {
        header: null,
        drawerLockMode: 'locked-closed'
    };
    constructor(props) {
        super(props);
        this.state = {

        }
    }


    async componentDidMount() {
        var user = firebase.auth().currentUser;
        const email = JSON.parse(await AsyncStorage.getItem("email"))
        console.log("Cameeeeee", email)

        if (email !== null) {
            this.props.navigation.navigate('Admin')
        }
        else if (user) {
            console.log("Cameeeeee")

            const collection = firebase.firestore().collection('Users')
            console.log("Cameeeeee")
            collection.doc(user._user.uid).get().then((success) => {
                if (success.exists) {
                    console.log("Cameeeeee")
                    this.props.navigation.navigate('Home')
                }
            })
        }
        BackHandler.addEventListener('hardwareBackPress', this.backPressed);
    }

    componentWillUnmount() {
        // removeListener()
        BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
    }


    backPressed = () => {
        Alert.alert(
            'Exit App',
            'Do you want to exit?',
            [
                { text: 'No' },
                { text: 'Yes', onPress: () => BackHandler.exitApp() },
            ],
            { cancelable: false });
        return true;
    }


    onPressLogin = async () => {
        Keyboard.dismiss()
        this.setState({ loading: true })
        let isValidPassword = (/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/).test(this.state.password)
        let isValidEM = (/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i).test(this.state.email)
        if (this.state.password === '' && this.state.email === '') {
            this.setState({ loading: false })
            this.refs.toast.show('Please enter all required fields');
        }
        else if (this.state.password === 'Admin@123' && this.state.email === 'admin@gmail.com') {
            firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
            .then(async(user) => {
                try {
                    await AsyncStorage.setItem("email", JSON.stringify(this.state.email));
                    this.setState({email:'',password:'',loading:false})
                } catch (error) {
                    // console.error('AsyncStorage#setItem error: ' + error.message);
                }
                this.props.navigation.navigate('Admin')
            })
           
        }
        else if (isValidPassword && isValidEM) {
            console.log("cuccess.data().active")
            firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
                .then((user) => {
                    var user = firebase.auth().currentUser;
                    if (user) {
                        const collection = firebase.firestore().collection('Users')
                        collection.doc(user._user.uid).get().then(async (success) => {
                            if (success.exists) {
                                if (success.data().active) {
                                    let data = success.data()
                                    data.loggedin = true
                                    collection.doc(user._user.uid).set(data).then(res => {
                                        this.setState({ loading: false,email:'',password:'' })
                                        this.props.navigation.navigate('Home')
                                    })
                                } else {
                                    alert("Please wait while your activation is being processed by admin")
                                    this.setState({ loading: false })
                                    firebase.auth().signOut().then(() => {
                                    }).catch((error) => {
                                        console.log('signout error', error);
                                    })
                                }
                            }
                        })
                    } else {
                        this.setState({ loading: false })
                    }
                }).catch((error) => {
                    this.setState({ loading: false })
                    alert(error)
                })
        }
        else if (!isValidEM) {
            this.setState({ loading: false })
            this.refs.toast.show('Please enter the valid email');

        }
        else if (!isValidPassword) {
            this.setState({ loading: false })
            this.refs.toast.show('Please enter the valid password');

        }
    }

    render() {
        const { loading, email, password } = this.state
        return (
            <Container style={styles.container}>
                <KeyboardAvoidingView enabled behavior='padding'>
                    <StatusBar barStyle="dark-content" backgroundColor="#0d98ba" />
                    {!this.state.apploading && <View>
                        <Text style={styles.heading}>LOGIN</Text>
                    </View>}
                    {!this.state.apploading && <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}>
                        <KeyboardAvoidingView style={{ width: '90%', alignItems: 'center', justifyContent: 'center' }}>
                            <Item style={{ borderColor: '#0d98ba', borderWidth: 2 }} regular>
                                <Input style={{ backgroundColor: '#ccf2ff' }} placeholderTextColor="#000066" placeholder="Enter your email" autoCapitalize='none' keyboardType="email-address" value={email} onChangeText={(text) => this.setState({ email: text })} />
                            </Item>
                            <Item regular style={{ borderColor: '#0d98ba', borderWidth: 2, marginTop: 10 }}>
                                <Input secureTextEntry={true} value={password} onChangeText={(text) => this.setState({ password: text })} style={{ backgroundColor: '#ccf2ff' }} placeholderTextColor="#000066" placeholder="Enter your password" />
                            </Item>

                            {!this.state.loading && <TouchableOpacity onPress={this.onPressLogin} style={styles.signInBig} >
                                <Text style={styles.signinText1}>SIGN IN</Text>
                            </TouchableOpacity>}
                            {this.state.loading && <View>
                                <Spinner color={'#0d98ba'} />
                            </View>}

                            {!this.state.loading && <Text onPress={() => this.props.navigation.navigate('Register')} style={styles.signupStyle}>Create a new account?</Text>}
                        </KeyboardAvoidingView>
                    </View>}
                    {this.state.apploading && <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <Spinner color={'#0d98ba'} />
                    </View>}
                </KeyboardAvoidingView>
                <Toast ref="toast" />
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#66d9ff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    content: {
        paddingHorizontal: '2%',
        paddingVertical: '20%'
    },
    socialbuttonstyle: {
        width: '86%',
        borderRadius: 3,
        height: '15%',
        justifyContent: 'center',
        backgroundColor: '#00ace6',
        marginBottom: '5%'
    },
    signInBig: {
        width: '100%',
        marginTop: 25,
        borderRadius: 10,
        backgroundColor: '#0d98ba'
    },
    signinText1: {
        fontSize: 19,
        marginLeft: 30,
        marginRight: 30,
        marginTop: 8,
        marginBottom: 8,
        color: "#fff",
        textAlign: 'center',
        fontWeight: 'bold'
    },
    forgot: {
        fontSize: 19,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 8,
        marginBottom: 8,
        color: "#00ace6",
        textAlign: 'center',
        fontWeight: 'bold'
    },
    signupText: {
        width: '86%',
        borderRadius: 3,
        height: '15%',
        justifyContent: 'center',
        marginBottom: '10%'
    },
    heading: {
        color: '#ffffe6',
        fontSize: 25,
        textAlign: 'center',
        marginBottom: 30
    },
    textStyle: {
        textAlign: 'center',
        color: '#ffff',
        fontWeight: '500',
        fontSize: 18,
    },
    signupStyle: {
        textAlign: 'center',
        fontSize: 18,
        paddingTop: '5%',
        color: "#fff",
    },
})

