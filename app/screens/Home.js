import React, { Component } from "react";
import { StyleSheet, Text, View, Dimensions, TouchableOpacity, TouchableHighlight, BackHandler, AsyncStorage, Easing, Keyboard, StatusBar, Alert, Image, Linking, Platform, AppState ,PermissionsAndroid} from "react-native";
import { Container, Header, Title, Item, Input, Content, Card, CardItem, Grid, Col, Footer,Spinner} from "native-base";
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import firebase from 'react-native-firebase'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import RNFetchBlob from 'rn-fetch-blob';
import PhotoUpload from 'react-native-photo-upload';
export default class HomeScreen extends Component {
    static navigationOptions = {
        header: null,
        title: null
    };
    constructor(props) {
        super(props);
        this.state = {
            position: {},
            carepop: false,
            bodyForCare: '',
            sendRequest: false,
            token: '',  
            condition:'',
            photoURL:'',
            loading:false
        };
    }


    uploadProfilePic = (avatar) => {
        console.log("Cameeeeeeeee")
        this.setState({loading:true})
        const Blob = RNFetchBlob.polyfill.Blob
        // const fs = RNFetchBlob.fs
        // window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
        // window.Blob = Blob

        // const options = { quality: 0.5, base64: true };
        // const data = avatar
        let uri = avatar.data
        var x = Math.floor((Math.random() * 100000) + 1);
        let imageName = x.toString() + ".jpg"
        let mime = 'image/jpg'
        // const uploadImage = (uri, imageName, mime = 'image/jpg') => {
        // return new Promise((resolve, reject) => {
        const uploadUri = uri
        let uploadBlob = null
        const imageRef = firebase.storage().ref('Profile').child(imageName)
        // fs.readFile(uploadUri, 'base64')
        //   .then((avatar) => {
        Blob.build(avatar, { type: `${mime};BASE64` })
            //   })
            .then((blob) => {
                uploadBlob = blob
                return imageRef.put(blob._ref, { contentType: mime })
            })
            .then(() => {
                uploadBlob.close()
                return imageRef.getDownloadURL()
            })
            .then((url) => {
                this.setState({
                    photoURL: url,
                    loading:false
                })
            }).catch((error) => {
                // console.log(" - - -error --- >  ", error)
            })
    }

    Permission = async () => {
        try {
            const internal = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE)
            const external = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
            const camera = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA)
            if (internal === 'granted' && external === 'granted' && camera === 'granted') {
                this.setState({ granted: true })
            } else {
                this.setState({ granted: false })
            }
        } catch (err) {
            console.warn(err)
        }
    }



    async componentWillmount() {
        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
            // user has permissions
        } else {
            // user doesn't have permission
            try {
                await firebase.messaging().requestPermission();
                // User has authorised
            } catch (error) {
                // User has rejected permissions
            }
        }
        BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
    }

    backPressed = () => {
        Alert.alert(
            'Exit App',
            'Do you want to exit?',
            [
                { text: 'No' },
                { text: 'Yes', onPress: () => BackHandler.exitApp() },
            ],
            { cancelable: false });
        return true;
    }

    getLocation = () => {
        console.log("======>>>>0")
        navigator.geolocation.getCurrentPosition(
            (position) => {
                console.log("======>>>>1", position)
                this.setState({
                    position: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        latitudeDelta: 0.015,
                        longitudeDelta: 0.0121
                    }
                })
            },
            (error) => {

            },
            { enableHighAccuracy: true, timeout: 10000, maximumAge: 2000 })
        navigator.geolocation.getCurrentPosition(
            (position) => {
                console.log("======>>>>2", position)
                this.setState({
                    position: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        latitudeDelta: 0.015,
                        longitudeDelta: 0.0121
                    }
                })
            },
            (error) => {
            },
            { enableHighAccuracy: false, timeout: 10000, maximumAge: 2000 })
    }

    async  componentDidMount() {
        this.Permission()
        this.getLocation()
        this.onTokenRefreshListener = firebase.messaging().onTokenRefresh(fcmToken => {
            // Process your token as required
        });
        const fcmToken = await firebase.messaging().getToken();
        if (fcmToken) {
            var user = firebase.auth().currentUser;
            if (user) {
                console.log("========>>>>>>", fcmToken)
                this.setState({ token: fcmToken })
                const collection = firebase.firestore().collection('Users')
                collection.doc(user._user.uid).get().then((success) => {
                    if (success.exists) {
                        let data = success.data()
                        data.token = fcmToken
                        collection.doc(user._user.uid).set(data).then(res => {
                        })
                    }
                })
            }
            // user has a device token
        } else {
            console.log("========>>>>>>user doesn't have a device token yet")
            // user doesn't have a device token yet
        }
        this.messageListener = firebase.messaging().onMessage((message) => {

            console.log("mess=======>>>>>>>", message)
            // Process your message as required
        });
        this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification) => {
            // Process your notification as required
            // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
            console.log("nnotificationDisplayedListener=======>>>>>>>", notification)

        });
        this.notificationListener = firebase.notifications().onNotification((notification) => {
            // Process your notification as required
            console.log("notificationListener=======>>>>>>>", notification)
            // this.showMessage()
            const channel = new firebase.notifications.Android.Channel('mychannelID', 'Test Channel', firebase.notifications.Android.Importance.Max)
                .setDescription('My apps test channel');
            // Create the channel
            firebase.notifications().android.createChannel(channel);
            const fireNotification = new firebase.notifications.Notification({ show_in_foreground: true })
                .setNotificationId(notification._notificationId)
                .setTitle(notification._title)
                .setBody(notification._body)
                .setData(notification._data)
                .android.setAutoCancel(true)
                .android.setChannelId('mychannelID')
                .android.setPriority(firebase.notifications.Android.Priority.High);
            firebase.notifications().displayNotification(fireNotification)
        });

        this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
            // Get the action triggered by the notification being opened
            console.log("notificationOpen=======>>>>>>>", notificationOpen)
            this.props.navigation.navigate("MapScreen", { data: notificationOpen.notification._data })

            const action = notificationOpen.action;
            // Get information about the notification that was opened
            const notification = notificationOpen.notification;
        });

        const notificationOpen = await firebase.notifications().getInitialNotification();
        if (notificationOpen) {
            console.log("notificationOpenInitial=======>>>>>>>", notificationOpen)
            this.props.navigation.navigate("MapScreen", { data: notificationOpen.notification._data })

            // App was opened by a notification
            // Get the action triggered by the notification being opened
            const action = notificationOpen.action;
            // Get information about the notification that was opened
            const notification = notificationOpen.notification;
        }

        BackHandler.addEventListener('hardwareBackPress', this.backPressed);

    }

    componentWillUnmount() {
        this.onTokenRefreshListener();
        this.messageListener();
        this.notificationDisplayedListener();
        this.notificationListener();
        this.notificationOpenedListener();
        BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
    }

    showMessage = () => {
        const channel = new firebase.notifications.Android.Channel('mychannelID', 'Test Channel', firebase.notifications.Android.Importance.Max)
            .setDescription('My apps test channel');

        // Create the channel
        firebase.notifications().android.createChannel(channel);
        const notification = new firebase.notifications.Notification()
            .setNotificationId("Dailynotif")
            .setTitle("Daily Reminder")
            .setBody("Time to Breath")
            .android.setAutoCancel(true)
            .android.setChannelId('mychannelID')
            .android.setPriority(firebase.notifications.Android.Priority.Max);
        firebase.notifications().displayNotification(notification)
    }

    logout = async () => {
        await AsyncStorage.removeItem('email');
        var user = firebase.auth().currentUser;
        if (user) {
            const collection = firebase.firestore().collection('Users')
            collection.doc(user._user.uid).get().then((success) => {
                if (success.exists) {
                    let data = success.data()
                    data.loggedin = false
                    collection.doc(user._user.uid).set(data).then(res => {
                        firebase.notifications().removeAllDeliveredNotifications()
                        firebase.notifications().cancelAllNotifications()
                        firebase.auth().signOut().then(() => {
                            this.props.navigation.navigate('Login')
                        }).catch((error) => {
                            console.log('signout error', error);
                        })
                    })
                }
            })
        }

    }
    renderHeader = () => (
        <View style={{ flex: 1, zIndex: 100, width: '100%', position: 'absolute' }}>

            <Header style={[style.headerView, { zIndex: -1 }]}>
                <StatusBar barStyle="light-content" backgroundColor="#0d98ba" />
                <TouchableOpacity>
                </TouchableOpacity>
                <Title style={style.headerTxt}>Home</Title>
                <TouchableOpacity onPress={this.logout}>
                    <MaterialCommunityIcons color='#fff' name="logout" size={25} />
                </TouchableOpacity>
            </Header >
        </View>
    );

    askCare = async () => {
        console.log("=======>>>>>askCare", this.state.bodyForCare)
        var user = firebase.auth().currentUser;
        if (user) {
            const collection = firebase.firestore().collection('Notifications')
            collection.doc(user._user.uid).set({
                'Authorization': 'key=AAAA3T5Ru6Q:APA91bFiwpzYHVkLXdMEFydwx5tLG9Nd8kCZdDZw7SkhV64gvhoP4u8EmdrhfiXeEOtbfuXYTcUms2euxxTf5xc_nymz0acsheMOXZOUMJbxbJfKRYc5hN4HzUi4820t8Uk6hUA0d0EJ',
                "body": {
                    "to": "cSWU4vR1UiA:APA91bGa9Rml5inZ6flLPohsNUc2bF9kpb6ab742M-4bSZUGVNmJYlWMbS_Bs0-6AUNSCuADBy3fbxq_DIToV73r-qWPN-uMEZKwIgkqD_9blgmODFim9linjvWbgk0Xi3j6bVPyAMWs",
                    "mutable-content": true,
                    "notification": {
                        "body": this.state.bodyForCare,
                        "title": "Need Care",
                        "click_action": "provider-body-panel"
                    },
                    "data": {
                        "token": this.state.token,
                        "userId": user._user.uid,
                        "email": user._user.email,
                        "position": this.state.position,
                        "body": this.state.bodyForCare,
                        "Authorization": 'key=AAAA3T5Ru6Q:APA91bFiwpzYHVkLXdMEFydwx5tLG9Nd8kCZdDZw7SkhV64gvhoP4u8EmdrhfiXeEOtbfuXYTcUms2euxxTf5xc_nymz0acsheMOXZOUMJbxbJfKRYc5hN4HzUi4820t8Uk6hUA0d0EJ',
                        "title": "Need Care",
                        "description": this.state.bodyForCare,
                        "status": "waiting",
                        "photoURL":this.state.photoURL
                    }
                },
                "status": "waiting",
                "condition":this.state.condition,
                "userId": user._user.uid,
                "email": user._user.email,
                "location": this.state.position,
                "photoURL":this.state.photoURL
            }).then((success) => {
                this.setState({ sendRequest: true })
            })
        }
    }

    render() {
        console.log("======>>>>>>", this.state)
        return (
            <Container style={style.container}>
                {this.renderHeader()}
              {this.state.position.latitude!==undefined &&  <View style={style.mapContainer}>
                    <MapView
                        provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                        style={style.map}
                        initialRegion={this.state.position}
                        zoomEnabled={true}
                        zoomControlEnabled={true}
                        showsUserLocation={true}
                    >
                        <Marker
                            coordinate={this.state.position}
                            title={"I am here"}
                            description={"Everyone is around to help you"}
                        />
                    </MapView>
                </View>}
                {this.state.carepop && <View style={{ flex: 1, zIndex: 100, alignItems: 'center', height: '100%', width: '100%', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0.5)', position: 'absolute' }} >
                    <View style={{ borderRadius: 20, height: '85%', width: '80%', alignItems: 'center', justifyContent: 'center', backgroundColor: '#fff' }}>
                    {!this.state.sendRequest &&  <TouchableOpacity onPress={()=>this.setState({ carepop: !this.state.carepop })} style={{ position: 'absolute', right: 4, top: 4 }}>
                                <MaterialCommunityIcons color='#0d98ba' name="close" size={30} />
                            </TouchableOpacity>}
                        {!this.state.sendRequest ? <View style={{ margin: 10, justifyContent: 'center', alignItems: 'center' }}>
                      {!this.state.loading ? <PhotoUpload
                                    
                                onResponse={(res) => {
                                    // console.log('resss')
                                }}
                                onStart={(start) => {
                                    // console.log('start', start)
                                }}
                                onCancel={(cancel) => {
                                    // console.log('cancell', cancel)
                                }}
                                onPhotoSelect={(avatar) => {
                                    if (avatar) {
                                        this.uploadProfilePic(avatar)
                                    }
                                }}
                            >
                                <Image
                                    style={{
                                        paddingVertical: 30,
                                        width: 150,
                                        height: 150,
                                        borderRadius: 75
                                    }}
                                    resizeMode='cover'
                                    source={{
                                        uri: (this.state.photoURL !== null && this.state.photoURL !== undefined && this.state.photoURL !== '' ? this.state.photoURL : 'https://www.sparklabs.com/forum/styles/comboot/theme/images/default_avatar.jpg')
                                    }}
                                />
                            </PhotoUpload>
                            :
                            <Spinner size={20} style={{color:'blue'}}/>}
                            <Text style={{ fontSize: 16,marginBottom:20 }}>You need a care from someone?</Text>
                            <Item regular>
                                <Input placeholder='Enter what you need?' onChangeText={(text) => this.setState({ bodyForCare: text })} />
                            </Item>
                            <TouchableOpacity style={{ borderRadius: 10, marginTop: 10, alignItems: 'center', justifyContent: 'center', backgroundColor: this.state.condition==='emergency'?'red': this.state.condition==='home'?"#ffff00ab":'green' }} onPress={this.askCare}>
                                <Text onPress={this.askCare} style={[style.footerTxt1,{color:this.state.condition==='home'?'#000': "#fff"}]}>Ask Care</Text>
                            </TouchableOpacity>
                        </View> : <View style={{ margin: 20, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontSize: 16 }}>Request Sent!</Text>
                                <TouchableOpacity style={{ borderRadius: 10, marginTop: 10, alignItems: 'center', justifyContent: 'center', backgroundColor: this.state.condition==='emergency'?'red': this.state.condition==='home'?"#ffff00ab":'green' }} onPress={this.askCare}>
                                    <Text onPress={() => this.setState({ carepop: false, sendRequest: false })} style={[style.footerTxt1,{color:this.state.condition==='home'?'#000': "#fff"}]}>Okay</Text>
                                </TouchableOpacity>
                            </View>}

                    </View>
                </View>}
                {!this.state.carepop &&  <TouchableOpacity onPress={()=>this.setState({ carepop: !this.state.carepop,condition:'emergency' })} style={{ flex: 1, zIndex: 100, bottom: 150,right:20,  position: 'absolute', alignItems: 'center', justifyContent: 'center', backgroundColor: '#fff',width:50,height:50,borderRadius: 25  }} >
                   
                            <TouchableOpacity style={{alignItems:'center',justifyContent:"center"}}  onPress={()=>this.setState({ carepop: !this.state.carepop,condition:'emergency' })}>
                                <MaterialCommunityIcons  onPress={()=>this.setState({ carepop: !this.state.carepop,condition:'emergency' })} color='red' name="ambulance" size={30} />
                            </TouchableOpacity>
                           
                        </TouchableOpacity>}
                        {!this.state.carepop &&  <TouchableOpacity onPress={()=>this.setState({ carepop: !this.state.carepop , condition:'home' })} style={{ flex: 1, zIndex: 100, bottom: 250,right:20,  position: 'absolute', alignItems: 'center', justifyContent: 'center', backgroundColor: '#fff',width:50,height:50,borderRadius: 25  }} >
                   
                   <TouchableOpacity onPress={()=>this.setState({ carepop: !this.state.carepop , condition:'home' })}  style={{alignItems:'center',justifyContent:"center"}}  onPress={()=>this.setState({ carepop: !this.state.carepop , condition:'home' })}>
                       <MaterialCommunityIcons onPress={()=>this.setState({ carepop: !this.state.carepop , condition:'home' })} color='#ffff00ab' name="home" size={30} />
                   </TouchableOpacity>
               </TouchableOpacity>}
               {!this.state.carepop &&  <TouchableOpacity  onPress={()=>this.setState({ carepop: !this.state.carepop , condition:'soceity' })} style={{ flex: 1, zIndex: 100, bottom: 350,right:20,  position: 'absolute', alignItems: 'center', justifyContent: 'center', backgroundColor: '#fff',width:50,height:50,borderRadius: 25  }} >
                   
                   <TouchableOpacity style={{alignItems:'center',justifyContent:"center"}}  onPress={()=>this.setState({ carepop: !this.state.carepop , condition:'soceity' })}>
                       <Ionicons   onPress={()=>this.setState({ carepop: !this.state.carepop , condition:'soceity' })} color='green' name="ios-people" size={30} />
                   </TouchableOpacity>
                  
               </TouchableOpacity>}
                  
            </Container>
        );
    }
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%",
        backgroundColor: '#fff',
    },
    mapContainer: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    headerView: {
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#0d98ba',
    },
    headerTxt: {
        color: '#fff',
    },
    partyName: {
        fontSize: 15,
    },
    footerBtn: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#0d98ba'

    },
    footerTxt: {
        fontSize: 20,
        color: '#fff'
    },
    cardTxt: {
        fontSize: 15,
        fontWeight: '300',
        color: '#0d98ba'
    },
    detailsTxt: {
        fontSize: 15,
        color: '#000'
    },
    editBtn: {
        marginTop: 15,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderColor: '#0d98ba',
        borderWidth: 1,
        borderRadius: 15
    },
    button: {
        backgroundColor: "teal",
        paddingHorizontal: 20,
        paddingVertical: 15,
        marginVertical: 10,
        borderRadius: 10
    },
    footerTxt1: {
        fontSize: 18,
        textAlign: 'center',
        marginLeft: 30,
        marginRight: 30,
        marginTop: 10,
        marginBottom: 10,
        // fontWeight: 'bold',
        color: '#fff',
        backgroundColor: 'transparent',
    }
});

