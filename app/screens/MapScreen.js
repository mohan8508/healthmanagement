import React, { Component } from "react";
import { StyleSheet, Text, View, Dimensions, TouchableOpacity, TouchableHighlight, BackHandler, AsyncStorage, Easing, Keyboard, StatusBar, Alert, Image, Linking, Platform, AppState,TextInput } from "react-native";
import { Container, Header, Title, Item, Input, Content, Card, CardItem, Grid, Col, Footer } from "native-base";
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import firebase from 'react-native-firebase'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';

export default class MapScreen extends Component {
    static navigationOptions = {
        header: null,
        title: null
    };
    constructor(props) {
        super(props);
        this.state = {
            Authorization: "",
            body: {},
            position: {},
            title: '',
            description: '',
            email: '',
            sendRequest: false,
            userId: '',
            bodyForCare: '',
            token: '',
            myposition: {},
            status:"",
            feedback:"",
            photoURL:''
        };
    }

    getLocation = () => {
        console.log("======>>>>0")
        navigator.geolocation.getCurrentPosition(
            (position) => {
                console.log("======>>>>1", position)
                this.setState({
                    myposition: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        latitudeDelta: 0.015,
                        longitudeDelta: 0.0121
                    }
                })
            },
            (error) => {

            },
            { enableHighAccuracy: true, timeout: 10000, maximumAge: 2000 })
        navigator.geolocation.getCurrentPosition(
            (position) => {
                console.log("======>>>>2", position)
                this.setState({
                    myposition: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        latitudeDelta: 0.015,
                        longitudeDelta: 0.0121
                    }
                })
            },
            (error) => {
            },
            { enableHighAccuracy: false, timeout: 10000, maximumAge: 2000 })
    }

    componentWillMount() {
        this.getLocation()
        console.log("aaaaaaaaaa", this.props)
        let data = this.props.navigation.state.params.data
        console.log("aaaaaaaaaa", data)

        if (data !== null) {
            console.log("sssssdddd", data)
            this.setState({
                Authorization: data.Authorization,
                position: JSON.parse(data.location),
                title: data.title,
                description: data.description,
                email: data.email,
                userId: data.userId,
                token: data.token,
                status:data.status,
                photoURL:data.photoURL
            })
        }
        BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
    }

    backPressed = () => {
        Alert.alert(
            'Exit App',
            'Do you want to exit?',
            [
                { text: 'No' },
                { text: 'Yes', onPress: () => BackHandler.exitApp() },
            ],
            { cancelable: false });
        return true;
    }

    async  componentDidMount() {
        console.log("======>>>>>>", this.state)



        BackHandler.addEventListener('hardwareBackPress', this.backPressed);

    }

    componentWillUnmount() {

        BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
    }


    
    logout = async () => {
        await AsyncStorage.removeItem('email');
        var user = firebase.auth().currentUser;
        if (user) {
            const collection = firebase.firestore().collection('Users')
            collection.doc(user._user.uid).get().then((success) => {
                if (success.exists) {
                    let data = success.data()
                    data.loggedin = false
                    collection.doc(user._user.uid).set(data).then(res => {
                        firebase.notifications().removeAllDeliveredNotifications()
                        firebase.notifications().cancelAllNotifications()
                        firebase.auth().signOut().then(() => {
                            this.props.navigation.navigate('Login')
                        }).catch((error) => {
                            console.log('signout error', error);
                        })
                    })
                }
            })
        }

    }

    renderHeader = () => (
        <View style={{ flex: 1, zIndex: 100, width: '100%', position: 'absolute' }}>

            <Header style={[style.headerView, { zIndex: -1 }]}>
                <StatusBar barStyle="light-content" backgroundColor="#0d98ba" />
                <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
                    <MaterialCommunityIcons color='#fff' name="arrow-left" size={25} />
                </TouchableOpacity>

                <TouchableOpacity onPress={this.logout}>
                    <MaterialCommunityIcons color='#fff' name="logout" size={25} />
                </TouchableOpacity>
            </Header >
        </View>
    );

    giveCare = async () => {
        var user = firebase.auth().currentUser;
        if (user) {
            fetch('https://fcm.googleapis.com/fcm/send', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': this.state.Authorization

                },
                body: JSON.stringify(
                    {
                        "to": this.state.token,
                        "mutable-content": true,
                        "notification": {
                            "body": "From: " + user._user.email + " Message: " + this.state.bodyForCare,
                            "title": "Help is on the way",
                            "click_action": "provider-body-panel"

                        },
                        "data": {
                            "userId": user._user.uid,
                            "email": user._user.email,
                            "location": this.state.myposition,
                            "status": "finished",
                            "token": this.state.token,
                            "body": this.state.bodyForCare,
                            "Authorization": "key=AAAA3T5Ru6Q:APA91bFiwpzYHVkLXdMEFydwx5tLG9Nd8kCZdDZw7SkhV64gvhoP4u8EmdrhfiXeEOtbfuXYTcUms2euxxTf5xc_nymz0acsheMOXZOUMJbxbJfKRYc5hN4HzUi4820t8Uk6hUA0d0EJ",
                            "title": "Give Care",
                            "description": this.state.bodyForCare,
                        }
                    }
                )
            }).then((response) => response.json())
                .then((responseData) => {
                    console.log("inside responsejson");
                    console.log('response object:', responseData)
                    const collection = firebase.firestore().collection('Notifications')
                    collection.doc(this.state.userId).set({
                        'Authorization': this.state.Authorization,
                        "body":{
                            Authorization: this.state.Authorization,
                            position: this.state.location,
                            title: this.state.title,
                            description: this.state.description,
                            email: this.state.email,
                            userId: this.state.userId,
                            token: this.state.token,
                            status:this.state.status,
                            photoURL:this.state.photoUrl
                        },
                        "status": "finished",
                        "userId": this.state.userId,
                        "email": this.state.email,
                        "location": this.state.position,
                    }).then((success) => {
                        this.setState({ sendRequest: true })
                        this.props.navigation.navigate('Home')
                    })
                }).done();

        }
        // const message = new firebase.messaging.RemoteMessage()
        //     .setMessageId("111111111222233333")
        //     .setTo('cSWU4vR1UiA:APA91bGa9Rml5inZ6flLPohsNUc2bF9kpb6ab742M-4bSZUGVNmJYlWMbS_Bs0-6AUNSCuADBy3fbxq_DIToV73r-qWPN-uMEZKwIgkqD_9blgmODFim9linjvWbgk0Xi3j6bVPyAMWs@gcm.googleapis.com')
        //     .setData({
        //         key1: 'value1',
        //         key2: 'value2',
        //     });
        // // Send the message
        // firebase.messaging().sendMessage(message);
        // const collection = firebase.firestore().collection('Users')
        // collection.get().then(async (success) => {
        //     if (success.docs.length > 0) {
        //         success.docs.forEach((each) => {
        //             console.log("=======>>>>>",each)
        //             if(each.data().token!==undefined){
        //                 console.log("=======>>>>>",each.data().token)
        //                 const message = new firebase.messaging.RemoteMessage()
        //                 .setMessageId(each.data().token +)
        //                 .setTo('950233316260@gcm.googleapis.com')
        //                 .setData({
        //                     key1: 'value1',
        //                     key2: 'value2',
        //                 });
        //             // Send the message
        //             firebase.messaging().sendMessage(message);
        //             }
        //         })
        //     }
        // })
    }

    needCare = () => {
        this.setState({ carepop: !this.state.carepop })
    }

    feedback=()=>{
        if(this.state.feedback!==''){
            var user = firebase.auth().currentUser;
            if (user) {
                let feedbackCollection =firebase.firestore().collection('Feedback')
                feedbackCollection.add({
                    email:user._user.email,
                    date: new Date(),
                    feedback:this.state.feedback
                }).then(res=>{1
                    this.props.navigation.navigate('Home')
                })
            }
        }else{
            this.props.navigation.navigate('Home')
        }
    }

    render() {
        console.log("======>>>>>>", this.state)
        return (
            <Container style={style.container}>
                {this.renderHeader()}
                {this.state.position.latitude!==undefined &&     <View style={style.mapContainer}>
                    <MapView
                        provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                        style={style.map}
                        initialRegion={this.state.position}
                        zoomEnabled={true}
                        zoomControlEnabled={true}
                        showsUserLocation={true}
                    >
                        <Marker
                            coordinate={this.state.position}
                            title={this.state.email}
                            description={this.state.description}
                        />
                    </MapView>
                </View>}
                <View style={{ flex: 1, zIndex: 100, bottom: 20, alignItems: 'center', width: '100%', justifyContent: 'center', position: 'absolute' }} >
                   {this.state.status==="published" ? <View style={{ borderRadius: 20, height: 'auto', width: '80%', alignItems: 'center', justifyContent: 'center', backgroundColor: '#fff' }}>
                        <View style={{ margin: 20, justifyContent: 'center', alignItems: 'center' }}>
                        {this.state.photoURL!=='' && this.state.photoURL!==undefined && <Image
                                    style={{
                                        paddingVertical: 30,
                                        width: 150,
                                        height: 150,
                                        borderRadius: 75
                                    }}
                                    resizeMode='cover'
                                    source={{
                                        uri: (this.state.photoURL !== '' ? this.state.photoURL : 'https://www.sparklabs.com/forum/styles/comboot/theme/images/default_avatar.jpg')
                                    }}
                                />}
                            <Text style={{ fontSize: 16 ,marginTop:10}}>Can you take care of him/her?</Text>
                            <Item regular>
                                <Input placeholder='Type anything you want to say!' onChangeText={(text) => this.setState({ bodyForCare: text })} />
                            </Item>
                            <TouchableOpacity style={{ borderRadius: 10, marginTop: 10, alignItems: 'center', justifyContent: 'center', backgroundColor: '#0d98ba' }} onPress={this.giveCare}>
                                <Text onPress={this.giveCare} style={style.footerTxt1}>Send Care Message </Text>
                            </TouchableOpacity>
                        </View>
                    </View>:
                    <View style={{ borderRadius: 20, height: 'auto', width: '80%', alignItems: 'center', justifyContent: 'center', backgroundColor: '#fff' }}>
                        <View style={{ margin: 20, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 16,marginBottom:20 }}>Hey, you're taken care of</Text>
                            <Text style={{ fontSize: 16,marginTop:10, }}>Email : {this.state.email }</Text>
                            <Text style={{ fontSize: 16 }}> Description : {this.state.description}</Text>
                            <View style={{ width: '100%', marginTop: 10, padding: 20, borderColor: 'blue', borderWidth: 1, backgroundColor: '#0c0d0a0d' }}>
                                            <TextInput
                                                value={this.state.feedback}
                                                multiline={true}
                                                blurOnSubmit={true}
                                                placeholder={'Write your feedback'}
                                                onChangeText={(text) => { this.setState({ feedback: text}) }}
                                                underlineColorAndroid="transparent"
                                            />
                            </View>
                            <TouchableOpacity style={{ borderRadius: 10, marginTop: 10, alignItems: 'center', justifyContent: 'center', backgroundColor: '#0d98ba' }} onPress={this.giveCare}>
                                <Text onPress={()=>this.feedback()} style={style.footerTxt1}>Thanks!</Text>
                            </TouchableOpacity>
                        </View>
                    </View>}
                </View>
            </Container>
        );
    }
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%",
        backgroundColor: '#fff',
    },
    mapContainer: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    headerView: {
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#0d98ba',
    },
    headerTxt: {
        color: '#fff',
    },
    partyName: {
        fontSize: 15,
    },
    footerBtn: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#0d98ba'

    },
    footerTxt: {
        fontSize: 20,
        color: '#fff'
    },
    cardTxt: {
        fontSize: 15,
        fontWeight: '300',
        color: '#0d98ba'
    },
    detailsTxt: {
        fontSize: 15,
        color: '#000'
    },
    editBtn: {
        marginTop: 15,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderColor: '#0d98ba',
        borderWidth: 1,
        borderRadius: 15
    },
    button: {
        backgroundColor: "teal",
        paddingHorizontal: 20,
        paddingVertical: 15,
        marginVertical: 10,
        borderRadius: 10
    },
    footerTxt1: {
        fontSize: 18,
        textAlign: 'center',
        marginLeft: 30,
        marginRight: 30,
        marginTop: 10,
        marginBottom: 10,
        // fontWeight: 'bold',
        color: '#fff',
        backgroundColor: 'transparent',
    }
});

