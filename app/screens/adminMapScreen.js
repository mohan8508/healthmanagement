import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  TouchableHighlight,
  BackHandler,
  AsyncStorage,
  Easing,
  Keyboard,
  StatusBar,
  Alert,
  Image,
  Linking,
  Platform,
  AppState
} from "react-native";
import {
  Container,
  Header,
  Title,
  Item,
  Input,
  Content,
  Card,
  CardItem,
  Grid,
  Col,
  Footer
} from "native-base";
import MaterialCommunityIcons from "react-native-vector-icons/dist/MaterialCommunityIcons";
import Ionicons from "react-native-vector-icons/dist/Ionicons";
import firebase from "react-native-firebase";
import MapView, { PROVIDER_GOOGLE, Marker } from "react-native-maps";

export default class AdminMapScreen extends Component {
  static navigationOptions = {
    header: null,
    title: null
  };
  constructor(props) {
    super(props);
    this.state = {
      Authorization: "",
      body: {},
      position: {},
      title: "",
      description: "",
      email: "",
      sendRequest: false,
      userId: "",
      myposition: {}
    };
  }

  componentWillMount() {
    console.log("aaaaaaaaaa", this.props);
    let data = this.props.navigation.state.params.data;
    console.log("aaaaaaaaaa", data);

    if (data !== null) {
      console.log("aaaaaaaaaa", data);
      this.setState({
        Authorization: data.Authorization,
        body: data.body,
        position: data.location,
        title: data.body.notification.title,
        description: data.body.notification.body,
        email: data.email,
        userId: data.userId
      });
    }
    BackHandler.removeEventListener("hardwareBackPress", this.backPressed);
  }

  backPressed = () => {
    Alert.alert(
      "Exit App",
      "Do you want to exit?",
      [{ text: "No" }, { text: "Yes", onPress: () => BackHandler.exitApp() }],
      { cancelable: false }
    );
    return true;
  };

  async componentDidMount() {
    console.log("======>>>>>>", this.state);

    BackHandler.addEventListener("hardwareBackPress", this.backPressed);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.backPressed);
  }

  logout = async () => {
    await AsyncStorage.removeItem("email");
    firebase
      .auth()
      .signOut()
      .then(() => {
        this.props.navigation.navigate("Login");
      })
      .catch(error => {
        console.log("signout error", error);
      });
  };

  renderHeader = () => (
    <View style={{ flex: 1, zIndex: 100, width: "100%", position: "absolute" }}>
      <Header style={[style.headerView, { zIndex: -1 }]}>
        <StatusBar barStyle="light-content" backgroundColor="#0d98ba" />
        <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
          <MaterialCommunityIcons color="#fff" name="arrow-left" size={25} />
        </TouchableOpacity>

        <TouchableOpacity onPress={this.logout}>
          <MaterialCommunityIcons color="#fff" name="logout" size={25} />
        </TouchableOpacity>
      </Header>
    </View>
  );

  giveCare = () => {
    console.log("hehhehee");
    const users = firebase.firestore().collection("Users");
    users.get().then(success => {
      if (success.docs.length > 0) {
        success.docs.forEach(each => {
          if (each.data().token !== undefined && each.data().uid !== this.state.userId) {
            let body = this.state.body;
            body.data.status = "published";
            body.data.location = this.state.position;
            body.to = each.data().token;
            console.log("hellll", body);
            fetch("https://fcm.googleapis.com/fcm/send", {
              method: "POST",
              headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: this.state.Authorization
              },
              body: JSON.stringify(body)
            })
              .then(response => response.json())
              .then(responseData => {
                console.log("inside responsejson");
                console.log("response object:", responseData);
                const collection = firebase
                  .firestore()
                  .collection("Notifications");
                collection
                  .doc(this.state.userId)
                  .set({
                    Authorization: this.state.Authorization,
                    body: this.state.body,
                    status: "published",
                    userId: this.state.userId,
                    email: this.state.email,
                    location: this.state.position
                  })
                  .then(success => {
                    this.setState({ sendRequest: true });
                    this.props.navigation.navigate("Admin");
                  });
              })
              .done();
          }
        });
      }
    });

    // const message = new firebase.messaging.RemoteMessage()
    //     .setMessageId("111111111222233333")
    //     .setTo('cSWU4vR1UiA:APA91bGa9Rml5inZ6flLPohsNUc2bF9kpb6ab742M-4bSZUGVNmJYlWMbS_Bs0-6AUNSCuADBy3fbxq_DIToV73r-qWPN-uMEZKwIgkqD_9blgmODFim9linjvWbgk0Xi3j6bVPyAMWs@gcm.googleapis.com')
    //     .setData({
    //         key1: 'value1',
    //         key2: 'value2',
    //     });
    // // Send the message
    // firebase.messaging().sendMessage(message);
    // const collection = firebase.firestore().collection('Users')
    // collection.get().then(async (success) => {
    //     if (success.docs.length > 0) {
    //         success.docs.forEach((each) => {
    //             console.log("=======>>>>>",each)
    //             if(each.data().token!==undefined){
    //                 console.log("=======>>>>>",each.data().token)
    //                 const message = new firebase.messaging.RemoteMessage()
    //                 .setMessageId(each.data().token +)
    //                 .setTo('950233316260@gcm.googleapis.com')
    //                 .setData({
    //                     key1: 'value1',
    //                     key2: 'value2',
    //                 });
    //             // Send the message
    //             firebase.messaging().sendMessage(message);
    //             }
    //         })
    //     }
    // })
  };

  needCare = () => {
    this.setState({ carepop: !this.state.carepop });
  };

  render() {
    console.log("======>>>>>>", this.state);
    return (
      <Container style={style.container}>
        {this.renderHeader()}
        {this.state.position.latitude!==undefined &&   <View style={style.mapContainer}>
          <MapView
            provider={PROVIDER_GOOGLE} // remove if not using Google Maps
            style={style.map}
            initialRegion={this.state.position}
            zoomEnabled={true}
            zoomControlEnabled={true}
            showsUserLocation={true}
          >
            <Marker
              coordinate={this.state.position}
              title={this.state.email}
              description={this.state.description}
            />
          </MapView>
        </View>}
        <View
          style={{
            flex: 1,
            zIndex: 100,
            bottom: 20,
            alignItems: "center",
            width: "100%",
            justifyContent: "center",
            position: "absolute"
          }}
        >
          <View
            style={{
              borderRadius: 20,
              height: "auto",
              width: "80%",
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: "#fff"
            }}
          >
            <View
              style={{
                margin: 20,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Text style={{ fontSize: 16 }}>
                Publish this request to others?
              </Text>
              <TouchableOpacity
                style={{
                  borderRadius: 10,
                  marginTop: 10,
                  alignItems: "center",
                  justifyContent: "center",
                  backgroundColor: "#0d98ba"
                }}
                onPress={this.giveCare}
              >
                <Text onPress={this.giveCare} style={style.footerTxt1}>
                  Yes,Sure!
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Container>
    );
  }
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    backgroundColor: "#fff"
  },
  mapContainer: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: "flex-end",
    alignItems: "center"
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
  headerView: {
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#0d98ba"
  },
  headerTxt: {
    color: "#fff"
  },
  partyName: {
    fontSize: 15
  },
  footerBtn: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#0d98ba"
  },
  footerTxt: {
    fontSize: 20,
    color: "#fff"
  },
  cardTxt: {
    fontSize: 15,
    fontWeight: "300",
    color: "#0d98ba"
  },
  detailsTxt: {
    fontSize: 15,
    color: "#000"
  },
  editBtn: {
    marginTop: 15,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
    borderColor: "#0d98ba",
    borderWidth: 1,
    borderRadius: 15
  },
  button: {
    backgroundColor: "teal",
    paddingHorizontal: 20,
    paddingVertical: 15,
    marginVertical: 10,
    borderRadius: 10
  },
  footerTxt1: {
    fontSize: 18,
    textAlign: "center",
    marginLeft: 30,
    marginRight: 30,
    marginTop: 10,
    marginBottom: 10,
    // fontWeight: 'bold',
    color: "#fff",
    backgroundColor: "transparent"
  }
});
