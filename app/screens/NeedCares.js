import React, { Component } from "react";
import { StyleSheet, Text,ScrollView, View, Dimensions, TouchableOpacity, TouchableHighlight, BackHandler, AsyncStorage, Easing, Keyboard, StatusBar, Alert, Image, Linking, Platform, AppState } from "react-native";
import { Container, Header, Title, Content, Card, CardItem, Grid, Col, Footer } from "native-base";
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import firebase from 'react-native-firebase'
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
let title = ""
export default class NeedCares extends Component {
    static navigationOptions = {
        header: null,
        title: null
    };
    constructor(props) {
        super(props);
        this.state = {
            Users: [],
            type: ''
        };
    }


    componentWillmount() {

        BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
    }

    backPressed = () => {
        this.props.navigation.goBack(null)
        return true;
    }

    componentDidMount() {
        let type = this.props.navigation.state.params.type
        const collection = firebase.firestore().collection('Notifications')
        collection.get().then((success) => {
            if (success.docs.length > 0) {
                let array = []
                success.docs.forEach((each) => {
                    if (each.data().status === "waiting") {
                        title = "Need Cares"
                            array.push(each.data())
                    }
                })
                if (array.length === 0) {
                    title = "No Needcare is found"
                }
                this.setState({ Users: array, type: type })
            }
        })
        BackHandler.addEventListener('hardwareBackPress', this.backPressed);

    }

    componentWillUnmount() {
        title = ""
        BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
    }


    logout = async () => {
        await AsyncStorage.removeItem('email');
        this.props.navigation.navigate('Login')
    }
    renderHeader = () => (
        <Header style={style.headerView}>
            <StatusBar barStyle="light-content" backgroundColor="#0d98ba" />
            {/* <TouchableOpacity onPress={this.openDrawer}>
                <MaterialCommunityIcons color='#fff' name="menu" size={25} />
            </TouchableOpacity> */}
            <Title style={style.headerTxt}>{this.state.type==="AllUsers"?"All Users":this.state.type==="PendingUsers"?"Pending Users":this.state.type==="logged"?"LoggedIn Users":this.state.type==="NeedCares"? "Need Cares":""}</Title>
            <TouchableOpacity onPress={this.logout}>
                <MaterialCommunityIcons color='#fff' name="logout" size={25} />
            </TouchableOpacity>
        </Header >
    );
    activate = (uid, index) => {
        const collection = firebase.firestore().collection('Users')
        let data = this.state.Users[index]
        data.active = true
        collection.doc(uid).set(data).then((success) => {
            alert("This user is activated")
            this.props.navigation.navigate("Admin")
        })
    }
    showlocation=(data)=>{
        this.props.navigation.navigate("AdminMapScreen",{ data: data })
    }

    render() {
        console.log("stateee", this.state)
        return (
            <Container style={style.container}>
                {this.renderHeader()}
                <ScrollView key={this.state.uniqueValue} style={{ width: '100%' }}>
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontSize: 20, marginTop: 10, marginBottom: 10, color: '#0d98ba', fontWeight: 'bold' }}>{title}</Text>
                    </View>
                    {this.state.Users.length > 0 && this.state.Users.map((data, index) => {
                        console.log("hhshshsh",data)
                        return (
                            <Card   >
                                <CardItem style={{ borderColor: '#0d98ba', borderWidth: 2, flexDirection: 'column' }}>
                                {data.photoURL!=='' && data.photoURL!==undefined && <Image
                                    style={{
                                        paddingVertical: 30,
                                        width: 150,
                                        height: 150,
                                        borderRadius: 75
                                    }}
                                    resizeMode='cover'
                                    source={{
                                        uri: (data.photoURL !== '' ? data.photoURL : 'https://www.sparklabs.com/forum/styles/comboot/theme/images/default_avatar.jpg')
                                    }}
                                />}
                                    <View style={{ flexDirection: 'row',marginTop:10 }}>
                                        <View style={{ width: '50%' }}><Text>Email</Text></View>
                                        <View style={{ width: '50%' }}><Text>{data.email}</Text></View>
                                    </View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ width: '50%' }}><Text>Descripion</Text></View>
                                        <View style={{ width: '50%' }}><Text>{data.body.notification.body}</Text></View>
                                    </View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ width: '50%' }}><Text>Status</Text></View>
                                        <View style={{ width: '50%' }}><Text>{data.status === 'waiting' ? "Pending Approval" : ""}</Text></View>
                                    </View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ width: '50%' }}><Text>Type</Text></View>
                                        <View style={{ width: '50%',flexDirection:'row',alignItems:'center' }}>
                                        <Text style={{color: data.condition==='emergency'?'red': data.condition==='home'?"#ffff00ab":'green' }}>{data.condition}</Text>
                                        {data.conditio==='soceity'?
                                        <Ionicons color='green' name="ios-people" size={30} />
                                    :
                                    <MaterialCommunityIcons color={data.condition==='emergency'?'red': data.condition==='home'?"#ffff00ab":'green' } name="earth" size={25} />
                                    }
                                        </View>
                                    </View>
                                    <TouchableOpacity onPress={() => this.showlocation(data)} style={{ position: 'absolute', right: 5, top: 5 }}>
                                        <MaterialCommunityIcons color='#0d98ba' name="earth" size={25} />
                                    </TouchableOpacity>
                                 
                                </CardItem>
                            </Card>
                        )
                    })}
                </ScrollView>
            </Container>
        );
    }
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%",
        backgroundColor: '#fff',
    },
    Mapcontainer: {
        ...StyleSheet.absoluteFillObject,
        height: 400,
        width: 400,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    headerView: {
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#0d98ba',
    },
    headerTxt: {
        color: '#fff',
    },
    partyName: {
        fontSize: 15,
    },
    footerBtn: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#0d98ba'

    },
    footerTxt: {
        fontSize: 20,
        color: '#fff'
    },
    cardTxt: {
        fontSize: 15,
        fontWeight: '300',
        color: '#0d98ba'
    },
    detailsTxt: {
        fontSize: 15,
        color: '#000'
    },
    editBtn: {
        marginTop: 15,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderColor: '#0d98ba',
        borderWidth: 1,
        borderRadius: 15
    },
    button: {
        backgroundColor: "teal",
        paddingHorizontal: 20,
        paddingVertical: 15,
        marginVertical: 10,
        borderRadius: 10
    },
    footerTxt1: {
        fontSize: 18,
        textAlign: 'center',
        marginLeft: 30,
        marginRight: 30,
        marginTop: 10,
        marginBottom: 10,
        // fontWeight: 'bold',
        color: '#fff',
        backgroundColor: 'transparent',
    }
});

