import React, { Component } from "react";
import { StyleSheet, Text, View, Dimensions, TouchableOpacity, TouchableHighlight, BackHandler, AsyncStorage, Easing, Keyboard, StatusBar, Alert, Image, Linking, Platform, AppState } from "react-native";
import { Container, Header, Title, Content, Card, CardItem, Grid, Col, Footer } from "native-base";
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import firebase from 'react-native-firebase'
import { ScrollView } from "react-native-gesture-handler";

export default class Admin extends Component {
    static navigationOptions = {
        header: null,
        title: null
    };
    constructor(props) {
        super(props);
        this.state = {

        };
    }


    async componentWillMount() {
        const fcmToken = await firebase.messaging().getToken();
        if (fcmToken) {
            console.log("fcmmm========>>>>>>",fcmToken)
        }
        this.messageListener = firebase.messaging().onMessage((message) => {

            console.log("mess=======>>>>>>>", message)
            // Process your message as required
        });
        this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification: Notification) => {
            // Process your notification as required
            // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
            console.log("nnotificationDisplayedListener=======>>>>>>>", notification)

        });
        this.notificationListener = firebase.notifications().onNotification((notification) => {
            // Process your notification as required
            console.log("notificationListener=======>>>>>>>", notification)
            // this.showMessage()
            const channel = new firebase.notifications.Android.Channel('mychannelID', 'Test Channel', firebase.notifications.Android.Importance.Max)
                .setDescription('My apps test channel');
            // Create the channel
            firebase.notifications().android.createChannel(channel);
            const fireNotification = new firebase.notifications.Notification({ show_in_foreground: true })
                .setNotificationId(notification._notificationId)
                .setTitle(notification._title)
                .setBody(notification._body)
                .setData(notification._data)
                .android.setAutoCancel(true)
                .android.setChannelId('mychannelID')
                .android.setPriority(firebase.notifications.Android.Priority.High);
            firebase.notifications().displayNotification(fireNotification)
        });

        this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen: NotificationOpen) => {
            // Get the action triggered by the notification being opened
            console.log("notificationOpen=======>>>>>>>", notificationOpen)
            this.props.navigation.navigate("MapScreen", { data: notificationOpen.notification._data })

            const action = notificationOpen.action;
            // Get information about the notification that was opened
            const notification: Notification = notificationOpen.notification;
        });

        const notificationOpen: NotificationOpen = await firebase.notifications().getInitialNotification();
        if (notificationOpen) {
            console.log("notificationOpenInitial=======>>>>>>>", notificationOpen)

            // App was opened by a notification
            // Get the action triggered by the notification being opened
            const action = notificationOpen.action;
            // Get information about the notification that was opened
            const notification: Notification = notificationOpen.notification;
        }

        BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
    }

    backPressed = () => {
        Alert.alert(
            'Exit App',
            'Do you want to exit?',
            [
                { text: 'No' },
                { text: 'Yes', onPress: () => BackHandler.exitApp() },
            ],
            { cancelable: false });
        return true;
    }

    async  componentDidMount() {

        BackHandler.addEventListener('hardwareBackPress', this.backPressed);

    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
    }

    logout = async () => {
        await AsyncStorage.removeItem('email');
        firebase.auth().signOut().then(() => {
            this.props.navigation.navigate('Login')
        }).catch((error) => {
            console.log('signout error', error);
        })
    }
    renderHeader = () => (
        <Header style={style.headerView}>
            <StatusBar barStyle="light-content" backgroundColor="#0d98ba" />
            <TouchableOpacity onPress={this.openDrawer}>
            </TouchableOpacity>
            <Title style={style.headerTxt}>Admin</Title>
            <TouchableOpacity onPress={this.logout}>
                <MaterialCommunityIcons color='#fff' name="logout" size={25} />
            </TouchableOpacity>
        </Header >
    );

    render() {
        return (
            <Container style={style.container}>
                {this.renderHeader()}
                <ScrollView style={{ width: '100%' }}>

                    <Card style={{ flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Users', { type: "AllUsers" })} style={{ width: '50%', height: 250, borderColor: '#00ace6', borderWidth: 2, justifyContent: 'center', alignItems: 'center' }}>
                            <Text>All Users</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Users', { type: "PendingUsers" })} style={{ width: '50%', height: 250, borderColor: '#00ace6', borderWidth: 2, justifyContent: 'center', alignItems: 'center' }}>
                            <Text>Pending Users</Text>

                        </TouchableOpacity>
                    </Card>
                    <Card style={{ flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('NeedCares', { type: "NeedCares" })} style={{ width: '50%', height: 250, borderColor: '#00ace6', borderWidth: 2, justifyContent: 'center', alignItems: 'center' }}>
                            <Text>Need Cares</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Users', { type: "logged" })} style={{ width: '50%', height: 250, borderColor: '#00ace6', borderWidth: 2, justifyContent: 'center', alignItems: 'center' }}>
                            <Text>Logged in Users</Text>
                        </TouchableOpacity>
                    </Card>
                    <Card style={{ flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Feedback', { type: "feedback" })} style={{ width: '50%', height: 250, borderColor: '#00ace6', borderWidth: 2, justifyContent: 'center', alignItems: 'center' }}>
                            <Text>Feedback</Text>
                        </TouchableOpacity>
                    </Card>
                </ScrollView>
            </Container>
        );
    }
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%",
        backgroundColor: '#fff',
    },
    Mapcontainer: {
        ...StyleSheet.absoluteFillObject,
        height: 400,
        width: 400,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    headerView: {
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#0d98ba',
    },
    headerTxt: {
        color: '#fff',
    },
    partyName: {
        fontSize: 15,
    },
    footerBtn: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#0d98ba'

    },
    footerTxt: {
        fontSize: 20,
        color: '#fff'
    },
    cardTxt: {
        fontSize: 15,
        fontWeight: '300',
        color: '#0d98ba'
    },
    detailsTxt: {
        fontSize: 15,
        color: '#000'
    },
    editBtn: {
        marginTop: 15,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderColor: '#0d98ba',
        borderWidth: 1,
        borderRadius: 15
    },
    button: {
        backgroundColor: "teal",
        paddingHorizontal: 20,
        paddingVertical: 15,
        marginVertical: 10,
        borderRadius: 10
    },
    footerTxt1: {
        fontSize: 18,
        textAlign: 'center',
        marginLeft: 30,
        marginRight: 30,
        marginTop: 10,
        marginBottom: 10,
        // fontWeight: 'bold',
        color: '#fff',
        backgroundColor: 'transparent',
    }
});

