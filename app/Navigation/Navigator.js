import { StyleSheet, View, StatusBar, AsyncStorage } from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";
import Home from "../screens/Home";
import Login from "../screens/Login";
import Register from "../screens/Register";
import Admin from "../screens/Admin";
import Users from "../screens/Users";
import NeedCares from "../screens/NeedCares";
import Feedback from "../screens/Feedback";
import MapScreen from "../screens/MapScreen";
import AdminMapScreen from "../screens/AdminMapScreen";


StatusBar.setBarStyle('light-content', true);

const AppNavigator = createStackNavigator(
    {
        Login: {
            screen: Login
        },
        Home: {
            screen: Home
        },
        Register: {
            screen: Register
        },
        Admin: {
            screen: Admin
        },
        Users: {
            screen: Users
        },
        NeedCares: {
            screen: NeedCares
        }, 
        MapScreen: {
            screen: MapScreen
        },
        AdminMapScreen:{
            screen:AdminMapScreen
        },
        Feedback:{
            screen:Feedback
        }
    },
    {
        initialRouteName: "Login",
        headerMode: 'none',
    }
);

export default createAppContainer(AppNavigator);