import React from 'react';
import AppNavigator from './app/Navigation/Navigator';
import { Root } from "native-base";

const App = (props) => {
  console.disableYellowBox = true
  return (
      <Root>
        <AppNavigator />
      </Root>
  );
};

export default App;